<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// JonyGuan:: 解决php artisan migrate 报错问题：Specified key was too long error
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // JonyGuan:: 解决php artisan migrate 报错问题：Specified key was too long error
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
