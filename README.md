# 安装并使用 Laravel Passport 来请求授权令牌（OAuth2.0认证方式）

## 一、 安装 Passport

### 使用 Composer 依赖包管理器安装 Passport

	composer require laravel/passport
	
### 这句代码添加到 config/app.php 的 providers 数组

	Laravel\Passport\PassportServiceProvider::class,
	
### 项目目录根目录执行下面命令，来运行 Passport 自带的数据库迁移文件（用于自动创建 Passport 必需的客户端数据表和令牌数据表）
	
	php artisan migrate
	
### 返回的结果：

	Migration table created successfully.
	Migrating: 2014_10_12_000000_create_users_table
	Migrated:  2014_10_12_000000_create_users_table
	Migrating: 2014_10_12_100000_create_password_resets_table
	Migrated:  2014_10_12_100000_create_password_resets_table
	Migrating: 2016_06_01_000001_create_oauth_auth_codes_table
	Migrated:  2016_06_01_000001_create_oauth_auth_codes_table
	Migrating: 2016_06_01_000002_create_oauth_access_tokens_table
	Migrated:  2016_06_01_000002_create_oauth_access_tokens_table
	Migrating: 2016_06_01_000003_create_oauth_refresh_tokens_table
	Migrated:  2016_06_01_000003_create_oauth_refresh_tokens_table
	Migrating: 2016_06_01_000004_create_oauth_clients_table
	Migrated:  2016_06_01_000004_create_oauth_clients_table
	Migrating: 2016_06_01_000005_create_oauth_personal_access_clients_table
	Migrated:  2016_06_01_000005_create_oauth_personal_access_clients_table	
	
### 来创建生成安全访问令牌时用到的加密密钥及私人访问和密码访问客户端。

	php artisan passport:install
	
### 返回的结果：

	Encryption keys generated successfully.
	Personal access client created successfully.
	Client ID: 1
	Client Secret: iG3Q9Dgv4CmUw3bMVFJIxru5nOPOaMMOqkB4Hu5Y
	Password grant client created successfully.
	Client ID: 2
	Client Secret: zaTJ4tfT6lPN1j9yLLwtluKJ18gaW7PHzNy4NKAt
	
### 将下面代码 Trait 添加到 App\User 模型中，这个 Trait 会给这个模型提供一些辅助函数，用于检查已认证用户的令牌和使用作用于。

	//JonyGuan:: 加在 namespace App; 下面
	use Laravel\Passport\HasApiTokens;

### 找到use Notifiable;在其后面添加 HasApiTokens
	
	//use Notifiable; // -> 这个是原来的，下面的是新调整后的
	use Notifiable,HasApiTokens;

### 打开\app\Providers\AuthServiceProvider.php 先在头部添加上	
	use Laravel\Passport\Passport; 

### 再找到boot()在其下增加一行 ，来注册 Passport 在访问令牌、客户端、私人访问令牌的发放和吊销过程中一些必要路由。

	Passport::routes();
	
### 最后修改下配置文件 config/auth.php 中的 api driver 改为 passport，此项操作会将 API 请求时使用 Passport 的  TokenGuard 来处理授权保护。打开\app\config\auth.php 找到 'guards'数组 修改如下：

###原来的：

	'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'token',
            'provider' => 'users',
        ],
    ],
    
### 修改后的：

	'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],
    ],


### 全部配置好后，我们来请求令牌，但是发现我们并没有创建任何用户，于是我们需要用到seed来预设填充数据。

	php artisan make:seeder UsersTableSeeder
	
### 创建一个 Seeder

#### 然后打开 database\seeds 文件夹里已经新建了一个叫 UserTableSeeder.php 的文件，打开它然后在 run 方法内输入以下代码：

	<?php
	use Illuminate\Database\Seeder;
	class UsersTableSeeder extends Seeder
	{
	    /**
	     * Run the database seeds.
	     *
	     * @return void
	     */
	    public function run()
	    {
	        DB::table('users')->insert([
				'name'=>'admin',
				'email'=>'admin@xiaoguan.com',
				'password'=>bcrypt('admin')
			]);
	    }
	}

### 执行下面命令，来往 user 表中填充刚刚添加好的 seed 数据。

	php artisan db:seed --class=UsersTableSeeder
	

# 二、JavaScript 请求授权令牌

### 修改默认首页模板 \resources\views\welcome.blade.php

	<!doctype html>
	<html lang="{{ app()->getLocale() }}">
	   <head>
	       <meta charset="utf-8">
	       <meta http-equiv="X-UA-Compatible" content="IE=edge">
	       <meta name="viewport" content="width=device-width, initial-scale=1">
	
	       <title>Laravel Passport 实战</title>
	       <link href="http://laravelpassport.loc/css/app.css" rel="stylesheet">
	
	       <style>
	           .m-t-md{
	               margin-top: 30px;
	           }
	       </style>
	
	   </head>
	   <body>
	       <div class="flex-center position-ref full-height">
	           <div class="content">
	
	               <div class="container m-t-md">
	                   <div class="row">
	                       <div class="col-md-8 col-md-offset-2">
	                           <div class="panel panel-default">
	                               <div class="panel-heading">Laravel Passport 实战</div>
	
	                               <div class="panel-body">
	                                   <form class="form-horizontal" method="POST" action="#">
	                                       <div class="form-group">
	                                           <label for="username" class="col-md-4 control-label">用户名</label>
	
	                                           <div class="col-md-6">
	                                               <input id="username" type="text" class="form-control" name="password" required>
	                                           </div>
	                                       </div>
	                                       <div class="form-group">
	                                           <label for="password" class="col-md-4 control-label">密码</label>
	
	                                           <div class="col-md-6">
	                                               <input id="password" type="password" class="form-control" name="password" required>
	                                           </div>
	                                       </div>
	
	                                       <div class="form-group">
	                                           <div class="col-md-8 col-md-offset-4">
	                                               <button type="button" id="loginButton" class="btn btn-primary">
	                                                   登录
	                                               </button>
	                                           </div>
	                                       </div>
	                                   </form>
	                               </div>
	                           </div>
	                       </div>
	                   </div>
	               </div>
	
	           </div>
	       </div>
	       <!-- Scripts -->
	       <script src="{{ asset('js/app.js') }}"></script>
	       <script>
	           $('#loginButton').click(function(event){
	               event.preventDefault();
	               $.ajax({
	                   type:'post',
	                   url:'/oauth/token',
	                   dataType:'json',
	                   data:{
	                       'grant_type':'password',
	                       'client_id':'2',
	                       //JonyGuan:: 注意这个里填写的client_secret是你php artisan passport:install生成的，注意匹配
	                       'client_secret':'zaTJ4tfT6lPN1j9yLLwtluKJ18gaW7PHzNy4NKAt',
	                       'username':$('#username').val(),
	                       'password':$('#password').val(),
	                       'scope':''
	                   },
	                   success:function(data){
	                       console.log(data);
	                       alert(JSON.stringify(data));
	                   },
	                   error:function(err){
	                       console.log(err);
	                       alert('statusCode:'+err.status+'\n'+'statusText:'+err.statusText+'\n'+'description:\n'+JSON.stringify(err.responseJSON));
	                   }
	               });
	           });
	       </script>
	   </body>
	</html>

### 注意 JS 部分JonyGuan::提示client_secret要修改成自己生成的

### 再将以下代码写入 User 模型 \app\User.php 找到 protected $hidden 数组 ，放其下面即可

	 	/**
	     * 自定义用Passport授权登录：用户名+密码
	     * @param $username
	     * @return mixed
	     */
	    public function findForPassport($username)
	    {
	        return self::where('name', $username)->first();
	    }

### 大功告成 访问首页 输入用户名 admin 和密码 admin ，点击登录按钮尝试登录：

	{
		"token_type": "Bearer",
		"expires_in": 31536000,
		"access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjUzNjg4Y2RiOWQ5ZGQ4ZDA1MGYxNWI2ODY5Y2E3MTUyNTFhMzQ4YmMzMDM3NmNhYThiYzRjNzEyZGFiNzhmMWMwNjc4YTdmNzViZThmMWY4In0.eyJhdWQiOiIyIiwianRpIjoiNTM2ODhjZGI5ZDlkZDhkMDUwZjE1YjY4NjljYTcxNTI1MWEzNDhiYzMwMzc2Y2FhOGJjNGM3MTJkYWI3OGYxYzA2NzhhN2Y3NWJlOGYxZjgiLCJpYXQiOjE1MTYzNDQwMzksIm5iZiI6MTUxNjM0NDAzOSwiZXhwIjoxNTQ3ODgwMDM5LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.LTt8nUqw15bMaKPVTBQrzUt5H-Bjey_SlfXZqfBdbiWUMzT-LcXlbx9Akkqa9i-W117JO4Gz4SGAk6dDyeFnxivXzHWwNiRs7pAkkG4HVC64IZG3TyJflNN1HHWtRJzv0hhqG7rfZJnU6H226p5mws8pwr-MxC9O12y-XZiI6ibjGc_jV3REbTjHNt8c-48GjjWkzgnafYHAfFo0n0xpv3uuX-skWns7RL4ge7Id6F6s-hg_XUkHu8ClR8ektP0CiCnzZ7FhSgbV7HOPcA5RhSt9j50H3Ud-h9hhz61F0xmIEQFLG9fz7x_dToDmV19qvoJ3cP_5dM4uwaWTsjve_igTEje8ECrKyRQKs733J3111HYgjX_F54sVcRU7gmOv3gJK55VEff1ZP25HDEa7PDL2cK8d5MAnIqXp0d7Qa1CAxz5VYlIlpek4VfSAAr72J65mtm7jYUvqtD52i7sa4iHtq8Gq5jJB8-eosPhS5TKkJId23lFMoWz5ab8GhVVbLwo1MRJPRXS1mdXpr3XJ29cH8MOa6SspcCgcTSShZRgk9PHV8Oqzd9i2J-XcmUV1LfNuoghDmVahb9PN6-sSZBw17zi8kLpWq08srw_0MjU6GlbNJS8jfd9fx-2bAArn2Nex27yrvMuBUC_ZQntKT_ZH1aal5qrLjfy9DOxYNZI",
		"refresh_token": "def50200244da9705e8faf081915fa5a2a4542318fb2ff0649330d89c8340cdf20e3cec0ab09f4d2f6c1803eecf6d17a2c11b6c1796c1d4d5e2ef354bb986b21e4ae6ea17d4f412b6e75738ea3ae8e7ddf0eba2b2c0517d7aa7d0ad01eb39444bf971a76ade45e92fa290e368c00d2f0998feeff923e26b4006a21750a6226ac696909f245d868972ba176a9d6feebc768309f02ea035fb3f4b9ab098824ab0e63b751a4d528f01853a36a3d372ca03794077f2bd6a10af0dd0c6a286bdfd52d579fb87c990afd9e64e8ec01c40b2aa8e9af996f0eb034e7ea3bc9c64cdd35bbf6b6588826e75e41d9488a829602e40197d595cd09bbd89be862a33e6e1ea57a2b21b99c76ed9379e26209bc2b18e2bc99a77559a4232ecc987cccabac670d763a766ab1d397768e11c2a91fa821466ac97942792af6fcbb3a6fa71d0c7b2a82d46660e48a33b515ccbeafabdd5c02d9fbadc20c58c6b99f14b6276321522557ba"
	}
	


### 官方文档：
	
	https://laravel.com/docs/5.4/passport

### 如遇到问题可留言 QQ：309678100 大力水手
